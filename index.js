const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 3001;

app.use(express.urlencoded({extended:true}));
app.use(express.json());

app.listen(port,()=>console.log(`Server running at port: ${port}`));

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.9s26b.mongodb.net/s30?retryWrites=true&w=majority',{
	useNewUrlParser:true,
	useUnifiedTopology:true
}
);

let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));

db.once("open",()=>console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type:String,
		default:"pending"
	}
});

const Task = mongoose.model("Task", taskSchema)

app.post('/task',(req,res)=>{
	Task.findOne({name: req.body.name},(err,result)=>{
		if(result != null && result.name == req.body.name){
			return res.send('Duplicate task found')
		}else{
			let newTask = new Task({
			name:req.body.name
			})

			newTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New task created')
				}
			})
		}
	})
})

//-------------------- ADDON ACTIVITY ----------------------

app.get('/tasks',(req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({data:result})
		}
	})
})

//------------------ USER ADDON --------------------------


const userSchema = new mongoose.Schema({
	username:String,
	password:String
});

const User = mongoose.model("User", userSchema)

app.post('/signup',(req,res)=>{
	User.findOne({username:req.body.username},(err,result)=>{
		if(result != null){
			return res.send('Username exist!')
		}else if(req.body.username == ""){
			return res.send('Username must not be empty!')
		}else{
			let newUser = new User({
				username:req.body.username,
				password:req.body.password
			})

			//let newUser = new User;

			newUser.save((saveErr,saveUser)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New user created')
				}

			})
		}
	})
})

app.get('/user',(req,res)=>{
	User.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({data:result})
		}
	})
})